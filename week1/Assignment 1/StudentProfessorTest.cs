﻿namespace Assignment_1
{
    class MainClass
    {
        static void Main(string[] args)
        {
            var person = new Person();
            person.Greet();

            var student = new Student();
            student.SetAge(5);
            student.Greet();
            student.ShowAge();

            var teacher = new Teacher();
            teacher.SetAge(25);
            teacher.Greet();
            teacher.Explain();
        }
    }
}