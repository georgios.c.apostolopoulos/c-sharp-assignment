namespace Assignment_4;

public interface IVehicle
{
    public void Drive();

    public bool Refuel(int gas);
}