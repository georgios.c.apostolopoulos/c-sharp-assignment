namespace Assignment_4;

public class Car : IVehicle
{
    private int gas;
    
    public Car(int initialGas)
    {
        gas = initialGas;
    }

    public void Drive()
    {
        if (gas > 0)
        {
            Console.WriteLine("The car is Driving");
        }
        
    }

    public bool Refuel(int gasoline)
    {
        if (gasoline <= 0) return false;
        gas += gasoline;
        Console.WriteLine($"new value of gasoline is: {gas}");
        return true;

    }
}