﻿namespace Assignment_4;

class MainClass
{
    static void Main()
    {
        var myCar = new Car(0);
        
        Console.Write("Enter the amount of gasoline to refuel: ");
        if (!int.TryParse(Console.ReadLine(), out var refuelAmount)) return;
        if (myCar.Refuel(refuelAmount))
        {
            myCar.Drive();
        }
    }
}