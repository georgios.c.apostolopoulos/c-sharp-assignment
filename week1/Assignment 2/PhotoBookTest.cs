namespace Assignment_2
{
    class MainClass
    {
        static void Main(string[] args)
        {
            var defaultPhotoBook = new PhotoBook();
            var result = defaultPhotoBook.GetNumberPages();
            Console.WriteLine(result);
            
            var defaultPhotoBookCustom = new PhotoBook(24);
            result = defaultPhotoBookCustom.GetNumberPages();
            Console.WriteLine(result);
            
            var bigPhotoBook = new BigPhotoBook();
            result = bigPhotoBook.GetNumberPages();
            Console.WriteLine(result);
        }
    }
}