﻿namespace Assignment_3;

class MainClass
{
    static void Main()
    {

        Console.Write("Give a name to the dog: ");
        string name = Console.ReadLine();

        Dog myDog = new Dog();
        
        myDog.SetName(name);
        
        Console.WriteLine($"Dog's name: {myDog.GetName()}");
        myDog.Eat();
    }
}