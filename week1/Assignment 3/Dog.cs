namespace Assignment_3;

public class Dog : Animal
{
    public override void Eat()
    {
        Console.WriteLine($"{Name} is eating.");
    }
}