﻿using MyErp.Business.Interfaces;
using MyErp.DAL;

namespace MyErp.Business
{
    public class OrderGenericRepository : IOrderGenericRepository
    {
        public void Create(Order order)
        {
            ErpDB.Orders.Add(order);
        }

        public List<Order> GetAll()
        {
            return ErpDB.Orders.ToList();
        }
    }

    public class CustomerGenericRepository : ICustomerGenericRepository
    {
        public void Create(Customer customer)
        {
            ErpDB.Customers.Add(customer);
        }

        public List<Customer> GetAll()
        {
            return ErpDB.Customers.ToList();
        }
    }
}