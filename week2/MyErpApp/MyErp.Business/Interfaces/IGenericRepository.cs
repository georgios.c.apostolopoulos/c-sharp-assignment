using MyErp.DAL;

namespace MyErp.Business.Interfaces
{
    public interface IGenericRepository<T>
    {
        void Create(T entity);
        List<T> GetAll();
    }

    public interface IOrderGenericRepository : IGenericRepository<Order>
    {
        // Additional methods specific to order repository if needed
    }

    public interface ICustomerGenericRepository : IGenericRepository<Customer>
    {
        // Additional methods specific to customer repository if needed
    }
}

