﻿using MyErp.Business;
using MyErp.Business.Interfaces;
using MyErp.DAL;

namespace MyErp.Application
{
    class Program
    {

        static void Main(string[] args)
        {
            // Create instances of repositories
            IOrderGenericRepository orderRepository = new OrderGenericRepository();
            ICustomerGenericRepository customerRepository = new CustomerGenericRepository();
            
            // Create and save orders
            for (int i = 0; i < 5; i++)
            {
                Order order = new Order
                {
                    Id = i,
                    Drink = $"Drink {i}",
                    Food = $"Food {i}",
                };
                orderRepository.Create(order);
            }

            // Create and save customers
            for (int i = 0; i < 5; i++)
            {
                Customer customer = new Customer
                {
                    Name = $"Name {i}",
                };
                customerRepository.Create(customer);
            }
            
            // Retrieve all orders and customers
            var allOrders = orderRepository.GetAll();
            var allCustomers = customerRepository.GetAll();
            
            // Instantiate PrintingService
            var printingService = new PrintService.PrintService();

            // Print the first order and the first customer
            if (allOrders.Count > 0)
            {
                string firstOrderJson = printingService.Print(allOrders[0]);
                Console.WriteLine("First Order:");
                Console.WriteLine(firstOrderJson);
            }
            else
            {
                Console.WriteLine("No orders found.");
            }

            if (allCustomers.Count > 0)
            {
                string firstCustomerJson = printingService.Print(allCustomers[0]);
                Console.WriteLine("First Customer:");
                Console.WriteLine(firstCustomerJson);
            }
            else
            {
                Console.WriteLine("No customers found.");
            }
        }

    }
}