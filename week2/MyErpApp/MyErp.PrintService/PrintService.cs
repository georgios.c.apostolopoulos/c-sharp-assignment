﻿using Newtonsoft.Json;

namespace MyErp.PrintService
{
    public class PrintService
    {
        public string Print<T>(T input)
        {
            return JsonConvert.SerializeObject(input);
        }
    }
}

