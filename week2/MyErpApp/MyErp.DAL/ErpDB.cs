namespace MyErp.DAL
{
    public static class ErpDB
    {
        // Define Lists for Customer and Order
        public static List<Customer> Customers { get; } = new List<Customer>();
        public static List<Order> Orders { get; } = new List<Order>();
    }
}

