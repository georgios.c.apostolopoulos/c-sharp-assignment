﻿namespace MyErp.DAL
{
    public class Order
    {
        public int Id { get; set; }
        public string Drink { get; set; }
        public string Food { get; set; }
    }
}